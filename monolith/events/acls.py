import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(query):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={query}"

    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict['photos'][0]['src']['original']


# def get_weather_data(query):
#     headers = {"Authorization": OPEN_WEATHER_API_KEY}
#     geo_url = f"http://api.openwatchermap.org/geo/1.0/direct?q={query}&appid={OPEN_WEATHER_API_KEY}"

#     geo_response = requests.get(geo_url, headers=headers)
#     geo_dict = geo_response.json()
#     latitude = geo_dict[0]['lat']
#     longitude = geo_dict[0]['lon']

#     weather_url = f'https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}&units=imperial'
#     weather_response = requests.get(weather_url, headers=headers)
#     weather_dict = weather_response.json()

#     temperature = weather_dict["main"]["temp"]
#     return {
#         "temp": temperature,
#         "description": weather_dict["weather"][0]["description"]
#     }
def get_weather_data(city, state):

    url = "http://api.openweathermap.org/geo/1.0/direct"
    #set the parameters for the LON and LAT request
    params = {
        "q": f"{city},{state},US", # Query includes city, state, and country code (US)
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,#Adds the
    }
    response = requests.get(url, params=params)
    content = response.json()
    try:
        latitutde = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitutde,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = response.json()
    return {
        "description": content["weather"][0]["description"],
        "temperature": content["main"]["temp"],
    }